package org.vaadin.marcus.ui;

import com.vaadin.annotations.Push;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.navigator.Navigator;
import com.vaadin.server.Page;
import com.vaadin.server.Responsive;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Panel;
import com.vaadin.ui.UI;
import com.vaadin.ui.themes.ValoTheme;

import javax.servlet.annotation.WebServlet;

@Theme("todo")
@SuppressWarnings("serial")
@Push
public class TodoUI extends UI {

    private HorizontalLayout mainLayout = new HorizontalLayout();
    private ListSelectLayout listSelectLayout = new ListSelectLayout();
    private Panel contentPanel = new Panel();

    @WebServlet(value = "/*", asyncSupported = true)
    @VaadinServletConfiguration(productionMode = false, ui = TodoUI.class)
    public static class Servlet extends VaadinServlet {
    }

    @Override
    protected void init(VaadinRequest request) {
        Page.getCurrent().setTitle("Valo Todo");
        Responsive.makeResponsive(this);

        createMainLayout();
        setupNavigator();
        navigateToTodo("Inbox");
    }

    private void createMainLayout() {
        mainLayout.setSizeFull();
        contentPanel.setSizeFull();
        contentPanel.addStyleName(ValoTheme.PANEL_BORDERLESS);

        mainLayout.addComponents(listSelectLayout, contentPanel);
        mainLayout.setExpandRatio(contentPanel, 1);
        setContent(mainLayout);
    }

    private void setupNavigator() {
        Navigator navigator = new Navigator(this, contentPanel);
        navigator.addView(TodoView.NAME, TodoView.class);
        listSelectLayout.navigatorInited();
    }

    public static void navigateToTodo(String todoName) {
        UI.getCurrent().getNavigator().navigateTo(TodoView.NAME + "/" + todoName.replaceAll(" ", "_"));
    }

}
