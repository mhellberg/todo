package org.vaadin.marcus.ui;

import com.vaadin.data.util.BeanItem;
import com.vaadin.event.ShortcutAction;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.Page;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import org.vaadin.marcus.backend.Todo;
import org.vaadin.marcus.backend.TodoList;
import org.vaadin.marcus.backend.TodoService;

public class TodoView extends VerticalLayout implements View, TodoList.UpdateListener {

    public static final String NAME = "todo";
    private TodoList list;
    private ListLayout listLayout;
    TodoService service = TodoService.getInstance();

    public TodoView() {
        setWidth("100%");
        addStyleName("todo-view");
        setMargin(true);
        setSpacing(true);
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        String listName = event.getParameters().replaceAll("_", " ");
        list = service.getList(listName);
        if (list == null) {
            addComponent(new Label("Unknown list, please select another."));
        } else {
            Page.getCurrent().setTitle("Valo Todo: " + list.getName());
            createLayout();
            list.addUpdateListener(this);
            addDetachListener(d -> list.removeUpdateListener(this));
            listLayout.refresh();
        }
    }

    private void createLayout() {
        Label header = new Label(list.getName());
        header.addStyleName(ValoTheme.LABEL_H1);

        setMargin(true);
        setSpacing(true);

        NewItemForm form = new NewItemForm();
        listLayout = new ListLayout();

        addComponents(header, form, listLayout);
    }

    @Override
    public void updated() {
        listLayout.refresh();
    }


    class NewItemForm extends HorizontalLayout {
        NewItemForm() {
            setWidth("100%");
            setSpacing(true);

            TextField newItemField = new TextField();
            newItemField.setInputPrompt("Add new item");
            newItemField.setWidth("100%");


            Button addButton = new Button("Add");
            addButton.addClickListener(e -> {
                if (!newItemField.getValue().isEmpty()) {
                    list.addTodo(new Todo(newItemField.getValue()));
                    newItemField.setValue("");
                    addButton.setEnabled(false);
                }
            });
            addButton.setEnabled(false);
            addButton.setClickShortcut(ShortcutAction.KeyCode.ENTER);
            newItemField.addTextChangeListener(e -> addButton.setEnabled(!e.getText().isEmpty()));
            newItemField.setTextChangeEventMode(AbstractTextField.TextChangeEventMode.EAGER);

            addComponents(newItemField, addButton);
            setExpandRatio(newItemField, 1);
        }
    }

    class ListLayout extends VerticalLayout {

        ListLayout() {
            addStyleName("todo-list");
            setSpacing(true);
            setMargin(true);
        }

        void refresh() {
            getUI().access(() -> {
                        removeAllComponents();
                        list.getTodos().forEach(this::addItemLayout);
                    }
            );

        }

        private void addItemLayout(Todo todo) {
            HorizontalLayout itemLayout = new HorizontalLayout();
            itemLayout.setSpacing(true);
            itemLayout.setDefaultComponentAlignment(Alignment.MIDDLE_LEFT);
            BeanItem<Todo> item = new BeanItem<>(todo);

            CheckBox completed = new CheckBox();
            completed.setPropertyDataSource(item.getItemProperty("completed"));

            Label title = new Label(todo.getTitle());
            if (todo.isCompleted()) {
                title.addStyleName("completed");
            }

            itemLayout.addComponents(completed, title);

            addComponent(itemLayout);
        }
    }
}
