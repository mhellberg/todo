package org.vaadin.marcus.ui;

import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import org.vaadin.marcus.backend.Observable;
import org.vaadin.marcus.backend.TodoList;
import org.vaadin.marcus.backend.TodoService;

public class ListSelectLayout extends VerticalLayout implements Observable.UpdateListener, ViewChangeListener {

    private final VerticalLayout listButtons;

    TodoService service = TodoService.getInstance();

    public ListSelectLayout() {
        setWidth("300px");
        addStyleName(ValoTheme.MENU_ROOT);
        addStyleName("list-select-layout");

        listButtons = new VerticalLayout();
        listButtons.setWidth("100%");
        listButtons.setMargin(new MarginInfo(true, false, true, false));
        addComponent(listButtons);

        TextField addListField = new TextField();
        addListField.addStyleName("add-list-field");
        addListField.setInputPrompt("Add new list");
        addListField.addValueChangeListener(v -> {
            String listName = addListField.getValue();
            if (!listName.isEmpty()) {
                service.addList(listName);
                TodoUI.navigateToTodo(listName);
            }
            addListField.setValue("");
        });
        addListField.setWidth("90%");
        addComponent(addListField);
        setComponentAlignment(addListField, Alignment.MIDDLE_CENTER);

        service.addUpdateListener(this);
        addDetachListener(d -> service.removeUpdateListener(this));
    }

    @Override
    public void attach() {
        super.attach();
        refresh();
    }

    private void refresh() {
        getUI().access(() -> {
            listButtons.removeAllComponents();
            service.getLists().forEach(list -> listButtons.addComponent(new ListButton(list)));
        });
    }

    @Override
    public void updated() {
        refresh();
    }

    @Override
    public boolean beforeViewChange(ViewChangeEvent event) {
        return true;
    }

    @Override
    public void afterViewChange(ViewChangeEvent event) {
        if (!event.getParameters().isEmpty()) {
            listButtons.forEach(b -> {
                if (b instanceof ListButton) {
                    ((ListButton) b).setSelectedIfMatches(event.getParameters());
                }
            });
        }
    }

    public void navigatorInited() {
        UI.getCurrent().getNavigator().addViewChangeListener(this);
        addDetachListener(d-> UI.getCurrent().getNavigator().removeViewChangeListener(this));
    }


    class ListButton extends Button implements Observable.UpdateListener {
        private TodoList list;

        ListButton(TodoList list) {
            this.list = list;
            setHtmlContentAllowed(true);
            setWidth("100%");
            setPrimaryStyleName(ValoTheme.MENU_ITEM);
            addClickListener(c -> TodoUI.navigateToTodo(list.getName()));
            updateCaption();
            list.addUpdateListener(ListButton.this);
            addDetachListener(d -> list.removeUpdateListener(this));
        }

        private void updateCaption() {
            setCaption(list.getName() +
                    "<span class=\"valo-menu-badge\">" + list.getIncomplete() + "</span>");
        }

        @Override
        public void updated() {
            updateCaption();
        }

        public void setSelectedIfMatches(String name) {
            if(list.getUrlFriendlyName().equalsIgnoreCase(name)){
                addStyleName("selected");
            } else {
                removeStyleName("selected");
            }
        }
    }
}
