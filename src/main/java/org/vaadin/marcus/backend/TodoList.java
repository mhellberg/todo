package org.vaadin.marcus.backend;

import java.util.*;

public class TodoList extends Observable implements Observable.UpdateListener{
    private String name;
    private List<Todo> todos = new LinkedList<>();

    public TodoList(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public List<Todo> getTodos() {
        return Collections.unmodifiableList(todos);
    }

    public void addTodo(Todo todo) {
        todo.addUpdateListener(this);
        todos.add(todo);
        fireUpdateEvent();
    }

    public long getIncomplete() {
        return todos.stream().filter(t->!t.isCompleted()).count();
    }

    @Override
    public void updated() {
        fireUpdateEvent();
    }

    public String getUrlFriendlyName() {
        return getName().replaceAll(" ", "_");
    }
}
