package org.vaadin.marcus.backend;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class TodoService extends Observable {


    private static TodoService INSTANCE = new TodoService();

    public static TodoService getInstance() {
        return INSTANCE;
    }

    private List<TodoList> todoLists = new LinkedList<>();


    public TodoService() {
        todoLists.add(new TodoList("Inbox"));
    }

    public List<TodoList> getLists() {
        return Collections.unmodifiableList(todoLists);
    }

    public TodoList getList(String listName) {
        for (TodoList todoList : Collections.unmodifiableList(todoLists)) {
            if (todoList.getName().equalsIgnoreCase(listName)) {
                return todoList;
            }
        }
        return null;
    }

    public synchronized void addList(String name) {
        todoLists.add(new TodoList(name));
        fireUpdateEvent();
    }

}
