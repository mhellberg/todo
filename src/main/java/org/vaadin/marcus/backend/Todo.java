package org.vaadin.marcus.backend;

/**
 * Created by mhellber on 9/21/14.
 */
public class Todo extends Observable {

    private boolean completed;
    private String title;

    public Todo(String title) {
        this.title = title;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
        fireUpdateEvent();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        fireUpdateEvent();
        this.title = title;
    }

}
