package org.vaadin.marcus.backend;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public abstract class Observable {
    Set<UpdateListener> listeners = new HashSet<>();

    public void addUpdateListener(UpdateListener listener) {
        listeners.add(listener);
    }

    public void removeUpdateListener(UpdateListener listener) {
        listeners.remove(listener);
    }

    public void fireUpdateEvent() {
        Collections.unmodifiableSet(listeners).forEach(UpdateListener::updated);
    }

    public interface UpdateListener {
        public void updated();
    }
}
